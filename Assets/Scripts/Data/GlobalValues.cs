﻿using System.Collections.Generic;
using Util;

/*
 *
 *
 * @author Raph + Nethos
 */
public static class GlobalValues {

	public static VehicleParameter velocity = new VehicleParameter("Velocity", 0);
	public static VehicleParameter energy = new VehicleParameter("Energy", 0);

	/*
	 * The list of currently available variables of the vehicle.
	 * For example, this determines what can be used in a conditional command block.
	 */
	public static List<VehicleParameter> availableVariables = new List<VehicleParameter> { 
		velocity, 
		energy,
	};

	public static Action move = new Action("Move", 1f);
	public static Action powerUp = new Action("Engine Pow. +", 2f);
	public static Action powerDown = new Action("Engine Pow. -", -2f);
	public static Action shoot = new Action("Shoot", 5f);
	public static Action shield = new Action("Shield", 10f);

	/*
	 * The list of currently available actions for the vehicle.
	 * For example, this determines what can be used in an action block.
	 */
	public static List<Action> availableActions = new List<Action>{
		move,
		powerUp,
		powerDown,
		shoot,
		shield,
	};

	public static Event fire = new Event("Fire");
	public static Event explode = new Event("Explode");
	public static Event ennemy = new Event("Ennemy");
	public static Event block = new Event("Block");

	/*
	 * The list of currently available actions for the vehicle.
	 * For example, this determines what can be used in an action block.
	 */
	public static List<Event> availableEvents = new List<Event>{
		fire,
		explode,
		ennemy,
		block
	};
}
