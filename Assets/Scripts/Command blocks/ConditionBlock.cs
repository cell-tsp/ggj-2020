﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Util;

/*
 * IF
 *
 * @author Raph
 */
public class ConditionBlock : CommandBlock {


	#region PublicParameters



	#endregion


	#region InternalVariables

	/*
	 * false ->		"<"
	 * true ->		">"
	 */
	private bool comparisonOperator;

	/*
	 * The numeric value the selected variable will be compared to.
	 */
	private int valueToCompareTo;

	#endregion


	#region References

	/*
	 * A reference to the command block following this one in case condition is not met.
	 * It should be automatically updated when the player arranges its program.
	 */
	public CommandBlock defaultInstruction;

	/*
	 * A Dropdown proposing the different available variables to compare.
	 */
	private Dropdown availableVariablesDropdown;

	private string comparator;

	/*
	 * The InputField containing the value to compare to, set by the player.
	 */
	private InputField valueInputField;

	#endregion


	private void Start() {
		// Setting up references
		availableVariablesDropdown = GetComponentInChildren<Dropdown>();
		valueInputField = GetComponentInChildren<InputField>();

		// Initializing fields
		foreach (VehicleParameter parameter in GlobalValues.availableVariables) {
			availableVariablesDropdown.options.Add(new Dropdown.OptionData(parameter.name));
		}

	}


	public override void Execute() {

		float value = GlobalValues.availableVariables[availableVariablesDropdown.value].value;
		float toCompare = (float) Convert.ToInt16(valueInputField.text);

		comparator = GetComponentInChildren<CompareButtonState>().GetComparator();
		switch (comparator) {
		case "<":
			if (value < toCompare) {
				nextInstruction.Execute();
			}
			else{
				defaultInstruction.Execute();
			}
			break;

		case ">":
			if (value > toCompare) {
				nextInstruction.Execute();
			}
			else{
				defaultInstruction.Execute();
			}
			break;

		default:
			throw new InvalidOperationException();
			break;
		}



	}



}
