﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Util;

/*
 * IF
 *
 * @author Raph
 */
public class WhenBlock : CommandBlock {


	#region PublicParameters



	#endregion


	#region InternalVariables

	

	#endregion


	#region References

	/*
	 * A reference to the command block following this one in case condition is not met.
	 * It should be automatically updated when the player arranges its program.
	 */
	public EventBlock triggerEvent;
	
	#endregion


	private void Start() {
		// Setting up references

	}


	public override void Execute() {

		if (triggerEvent.isOccuring) {
			nextInstruction.Execute();
		}
		

	}



}
