﻿using System;
using UnityEngine;

/*
 * Superclass for all command blocks that can be manipulated in the algo view by the player.
 *
 * @author Raph
 */
public abstract class CommandBlock : MonoBehaviour {

    /*
	 * A reference to the command block preceding this one.
	 * It should be automatically updated when the player arranges its program.
	 */
    public CommandBlock previousInstruction;

    /*
	 * A reference to the command block following this one.
	 * It should be automatically updated when the player arranges its program.
	 */
    public CommandBlock nextInstruction;

    public abstract void Execute();
}
