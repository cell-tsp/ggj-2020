﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Util;

public class ActionBlock : CommandBlock
{

	#region InternalVariables

	/*
	 * The action executed by this ActionBlock
	 */
	private Action action;
	public GameObject ship;
	public Image shield;

	#endregion

	private void Start()
	{
		// Initializing fields
		
	}

	public override void Execute()
	{
		GlobalValues.availableVariables[1].value -= action.energyPerSec * Time.deltaTime;
		action.isOccuring = true;

		switch (action.name)
		{
			case "Move":
				Vector3 v3 = ship.transform.position;
				v3[0] += GlobalValues.availableVariables[0].value * Time.deltaTime;
				ship.transform.position = v3;
				break;

			case "Engine Pow. +":
				GlobalValues.availableVariables[0].value += 1;
				break;

			case "Engine Pow. -":
				GlobalValues.availableVariables[0].value -= 1;
				break;

			case "Shoot":
				break;

			case "Shield":
				shield.enabled = true;
				break;

			default:
				break;
		}
    }

}
