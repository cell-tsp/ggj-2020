﻿using System;
using UnityEngine;

/*
 *
 *
 * @author  Raph
 */
public class GameManager : MonoBehaviour {


	#region PublicParameters

	private static bool Wiring;
	private static string NeededNode; //either "In" or "Out"
	public static void SetWiring(bool b, string needed)
	{
		Debug.Log("Wiring is " + b + " and needs " + needed);
		Wiring = b;
		NeededNode = needed;
	}

	public static bool GetWiring()
	{
		return Wiring;
	}

	public static string GetNeeded()
	{
		return NeededNode;
	}

	#endregion


	#region CalculatedParameters



	#endregion


	#region InternalVariables

	#endregion


	#region References



	#endregion



	private void OnValidate() {
		// Calculating ...
		
	}

	private void Start() {
		// Setting up references


		// Initializing fields
		Wiring = false;

	}


	private void Update() {
		

	}



}
