﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompareButtonState : MonoBehaviour
{
    int currentState = 0;
    string[] states = { "<", ">" };

    Text text;

    private void Start()
    {
        text = GetComponent<Text>();
        updateButtonAppearance();
    }

    /*
     * Return the string representation of the current selected state.
     */
    public string GetComparator()
    {
        return states[currentState];
    }

    public void OnClick()
    {
        currentState++;
        currentState %= states.Length;
        updateButtonAppearance();
    }

    void updateButtonAppearance()
    {
        text.text = states[currentState];
    }
}
