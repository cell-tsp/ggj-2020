﻿namespace Util
{

    /*
    * Wrapper class for possible in-game actions
    * Examples: move, shoot, shield, ...
    *
    * @author Nethos
    */
    public class Action
    {

        public string name { get; }

        public float energyPerSec { get; }
        public bool isOccuring { get; set; }

        public Action(string name, float en)
        {
            this.name = name;
            energyPerSec = en;
            isOccuring = false;
        }


    }
}