﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WireScript : MonoBehaviour
{
    public BoxCollider2D In;
    public BoxCollider2D Out1;
    public BoxCollider2D Out2;

    public void OnMouseDown()
    {
        Debug.Log("MouseDown");
        if (!GameManager.GetWiring())
        {
            Vector2 v2 = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            if (In.OverlapPoint(v2))
            {
                GameManager.SetWiring(true, "Out");
            }
            else if (Out1.OverlapPoint(v2))
            {
                GameManager.SetWiring(true, "In");
            }
            else if (Out2 != null && Out2.OverlapPoint(v2))
            {
                GameManager.SetWiring(true, "In");
            }
        }
        else
        {
            string needed = GameManager.GetNeeded();
            Vector2 v2 = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            if (In.OverlapPoint(v2) && needed == "In")
            {

            }
            else if (Out1.OverlapPoint(v2) && needed == "Out")
            {

            }
            else if (Out2 != null && Out2.OverlapPoint(v2) && needed == "Out")
            {

            }

            GameManager.SetWiring(false, "");
        }
    }

}
