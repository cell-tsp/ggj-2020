﻿namespace Util {

 /*
 * Wrapper class for a parameter of the vehicle.
 * Examples: velocity, energy, attack power, ...
 *
 * @author Raph
 */
public class VehicleParameter {

	public string name { get; }

	public float value { get; set; }

	public VehicleParameter(string name, float value) {
		this.name = name;
		this.value = value;
	}


}
}
