﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseDrag : MonoBehaviour, IDragHandler, IBeginDragHandler
{
    public Camera cam;
    Vector3 offset;

    public int gridSize = 50;
    public Rigidbody2D rigidbody;

    private Vector3 lastPosition;

    public void OnBeginDrag(PointerEventData eventData)
    {
        lastPosition = transform.position;
        offset = transform.position - Input.mousePosition;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = placeOnGrid(Input.mousePosition + offset);
        bool collided = CheckForCollision();
        //if (collided)
        //{
        //    transform.position = lastPosition;
        //}
        lastPosition = transform.position;
    }

    public Vector3 placeOnGrid(Vector3 v3)
    {
        for (int i = 0; i < 3; i++)
        {
            v3[i] -= v3[i] % gridSize;
        }

        return v3;
    }

    public bool CheckForCollision()
    {
        Collider2D[] colliders = new Collider2D[5];
        int colliderNumber = rigidbody.GetContacts(colliders);

        return colliderNumber != 0;
    }

}
