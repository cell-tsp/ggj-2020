﻿namespace Util
{

    /*
    * Wrapper class for possible in-game events
    * Examples: explode, fire, ennemy, ...
    *
    * @author Nethos
    */
    public class Event
    {

        public string name { get; }
        public bool isOccuring { get; set; }

        public Event(string name)
        {
            this.name = name;
            this.isOccuring = false;
        }


    }
}